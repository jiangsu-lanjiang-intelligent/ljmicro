package route

import (
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/example/internal/service/gin/handler"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/service/xgin"
)

// InitUserRoute initializes the user route of the org service.
func InitUserRoute(e *xgin.GinService) {
	r := e.Group("user")
	{
		r.GET("/test", handler.TestUserHandler)
	}
}
