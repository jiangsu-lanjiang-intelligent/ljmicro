package main

import (
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/example/internal/service/gin/route"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/application"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/conf"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/loger"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/orm"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/service/xgin"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/xconsul"
)

func main() {
	application.AppendAssemblies(
		conf.Initialize,
		loger.Initialize,
		orm.Initialize,
		xconsul.Initialize,
	)
	application.Initialize("example")
	newXginService()
	application.KeepAlive()
}

func newXginService() {
	ginService := xgin.NewGinService()
	route.InitUserRoute(ginService)
	if err := ginService.OpenHealthApi(); err != nil {
		panic(err)
	}
	application.StartUpService(ginService)
}
