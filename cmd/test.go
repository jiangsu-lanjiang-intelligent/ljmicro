package main

import (
	"encoding/hex"
	"fmt"
	"github.com/jacobsa/go-serial/serial"
	"io"
	"log"
)

func main() {
	options := serial.OpenOptions{
		PortName:        "/dev/tty.usbserial-11330",
		BaudRate:        9600,
		DataBits:        8,
		StopBits:        1,
		MinimumReadSize: 4,
	}

	// Open the port.
	port, err := serial.Open(options)
	if err != nil {
		log.Fatalf("serial.Open: %v", err)
	}

	// Make sure to close it later.
	defer port.Close()

	for {
		n, err := port.Write([]byte{0x01, 0x03, 0x00, 0x04, 0x00, 0x01, 0xC5, 0xCB})
		if err != nil {
			log.Fatalf("port.Write: %v", err)
		}

		fmt.Println("Wrote", n, "bytes.")

		buf := make([]byte, 32)
		n, err = port.Read(buf)
		if err != nil {
			if err != io.EOF {
				fmt.Println("Error reading from serial port: ", err)
			}
		} else {
			buf = buf[:n]
			str := hex.EncodeToString(buf)
			fmt.Println("Rx: ", str)
			var h, l uint16
			h = uint16(buf[3])
			l = uint16(buf[4])
			fmt.Println("液位：", float64((h<<8)+l)/10.0)
		}
	}
}
