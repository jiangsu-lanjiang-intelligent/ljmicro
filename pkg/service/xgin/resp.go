package xgin

import (
	"fmt"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/conf"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/loger"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/utils/xerror"
	"github.com/gin-gonic/gin"
	"net/http"
	"reflect"
)

type Response struct {
	Code int         `json:"code"`
	Data interface{} `json:"data"`
	Msg  interface{} `json:"msg"`
}

const (
	SUCCESS       = 0
	BusinessError = 501 // 逻辑异常
	ERROR         = 500 // 服务器内部逻辑异常
	NoFind        = 404
	ERROR_PARAM   = 400 // 参数错误
)

func Result(c *gin.Context, ResCode int, ResMsg interface{}, ResData interface{}) {
	resp := gin.H{
		"code": ResCode,
		"msg":  ResMsg,
		"data": ResData,
	}
	c.JSON(http.StatusOK, resp)
	c.Abort()
}

func Ok(c *gin.Context) {
	Result(c, SUCCESS, "操作成功", nil)
}
func OkWithMessage(c *gin.Context, ResMsg interface{}) {
	Result(c, SUCCESS, ResMsg, nil)
}
func OkWithData(c *gin.Context, ResData interface{}) {
	Result(c, SUCCESS, "操作成功", ResData)
}
func OkDetailed(c *gin.Context, ResMsg interface{}, ResData interface{}) {
	Result(c, SUCCESS, ResMsg, ResData)
}

func Fail(c *gin.Context) {
	Result(c, ERROR, "系统异常，请稍后重试", nil)
}
func FailWithMessage(c *gin.Context, ResMsg interface{}) {
	Result(c, ERROR, ResMsg, nil)
}
func FailWithData(c *gin.Context, ResData interface{}) {
	Result(c, ERROR, "系统异常，请稍后重试", ResData)
}
func FailWithDetailed(c *gin.Context, ResCode int, ResMsg interface{}, ResData interface{}) {
	Result(c, ResCode, ResMsg, ResData)
}
func FailWithError(c *gin.Context, err error) {
	loger.WithContext(c).Error(fmt.Sprintf("[ERROR] (%s) %s", reflect.TypeOf(err).String(), err.Error()))
	switch e := err.(type) {
	case xerror.IError:
		if conf.GetString(conf.AppEnvKey) != "release" {
			Result(c, ERROR, e.Detail(), nil)
		} else {
			Result(c, ERROR, e.Info(), nil)
		}
	case *xerror.BusinessError:
		Result(c, BusinessError, e.Msg, e.Code)
	default:
		Result(c, ERROR, e.Error(), nil)
	}

}
