package xgin

import (
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/conf"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/service/xgin/middlerware"
	"github.com/gin-gonic/gin"
)

/*
链路日志-orm
*/

type GinService struct {
	*gin.Engine
}

func (g *GinService) Name() string {
	return "gin"
}

func (g *GinService) StartUp() error {
	return g.Run(":" + conf.GetStringd("port", "8080"))
}

func (g *GinService) Stop() {
}

// NewGinService 注册一个新的GinService
func NewGinService() *GinService {
	gin.SetMode(gin.ReleaseMode)
	e := gin.New()
	e.Use(
		middlerware.CorsMiddleware,
		middlerware.RecoveryMiddleware,
		middlerware.ReqIdMiddleware,
		middlerware.ReqLogMiddleware,
	)
	return &GinService{e}
}
