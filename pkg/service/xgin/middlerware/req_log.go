package middlerware

import (
	"fmt"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/loger"
	"github.com/gin-gonic/gin"
	"time"
)

// ReqLogMiddleware 请求日志中间件
func ReqLogMiddleware(c *gin.Context) {
	l := loger.WithContext(c)
	var (
		method   = c.Request.Method
		path     = c.Request.URL.Path
		protocol = c.Request.Proto
		duration = time.Now().Nanosecond()
		respCode int
	)
	c.Next()

	respCode = c.Writer.Status()
	duration = (time.Now().Nanosecond() - duration) / 1000000

	l.Info(fmt.Sprintf("%s %s %s %d %dms", method, path, protocol, respCode, duration))
}
