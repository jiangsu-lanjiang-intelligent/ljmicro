package middlerware

import (
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/loger"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

const (
	RequestId = "X-RequestId"
)

// ReqIdMiddleware req_id中间件
func ReqIdMiddleware(c *gin.Context) {
	reqId := ""
	if v := c.Request.Header.Get(RequestId); v != "" {
		reqId = v
	} else {
		reqId = uuid.New().String()
	}
	c.Set(loger.LableKeyReqId, reqId)
	c.Writer.Header().Add(RequestId, reqId)
	c.Next()
}
