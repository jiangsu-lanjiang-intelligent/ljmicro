package xgin

import (
	"github.com/gin-gonic/gin"
	"testing"
)

func TestNewGinService(t *testing.T) {
	e := NewGinService()
	e.GET("/hello", func(c *gin.Context) {
		c.String(200, "hello")
	})
	e.StartUp()
}
