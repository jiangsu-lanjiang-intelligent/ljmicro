package xgin

import (
	"fmt"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/xconsul"
	"github.com/gin-gonic/gin"
)

func (s *GinService) OpenHealthApi() error {
	s.Any("/health", func(c *gin.Context) {
		OkWithMessage(c, "health")
	})
	err := xconsul.RegisterService(
		xconsul.WithHealthCheck("/health"),
	)
	if err == xconsul.UnInitErr {
		fmt.Println("xconsul未初始化,无法注册服务")
		return nil
	}
	return err
}
