package conf

import (
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/utils/xcast"
)

type (
	// ConfDataSource 配置数据源
	ConfDataSource struct {
		items ConfigDataSourceItems
	}
	ConfigDataSourceItems []*ConfDataSourceItem
	// ConfDataSourceItem 配置数据源项
	ConfDataSourceItem struct {
		key    string
		value  interface{}
		childs ConfigDataSourceItems
	}
)

func newConfDataSource() *ConfDataSource {
	return &ConfDataSource{
		items: ConfigDataSourceItems{},
	}
}

func (s ConfigDataSourceItems) SetConfig(key []string, value interface{}) ConfigDataSourceItems {
	var find_key = func(s *ConfDataSourceItem) {
		if len(key) > 1 {
			if s.childs == nil {
				s.childs = ConfigDataSourceItems{}
			}
			s.childs = s.childs.SetConfig(key[1:], value)
		} else {
			s.value = value
		}
	}
	for _, t := range s {
		if t.key == key[0] {
			find_key(t)
			return s
		}
	}
	ds := &ConfDataSourceItem{
		key: key[0],
	}
	find_key(ds)
	s = append(s, ds)
	return s
}
func (s ConfigDataSourceItems) find(keys []string) *ConfDataSourceItem {
	for _, t := range s {
		if t.key == keys[0] {
			if len(keys) > 1 {
				return t.childs.find(keys[1:])
			} else {
				return t
			}
		}
	}
	return &ConfDataSourceItem{}
}
func (s ConfigDataSourceItems) Get(key string) interface{} {
	return s.find(splitConfigKey(key)).value
}
func (s ConfigDataSourceItems) GetString(key string) string {
	return xcast.ToString(s.Get(key))
}
func (s ConfigDataSourceItems) GetStringd(key, dv string) string {
	v := s.GetString(key)
	if v == "" {
		return dv
	}
	return v
}
func (s ConfigDataSourceItems) GetBool(key string) bool {
	return xcast.ToBool(s.Get(key))
}
func (s ConfigDataSourceItems) GetBoold(key string, dv bool) bool {
	if v := s.Get(key); v == nil {
		return dv
	} else {
		return xcast.ToBool(dv)
	}
}
func (s ConfigDataSourceItems) GetInt(key string) int {
	return xcast.ToInt(s.Get(key))
}
func (s ConfigDataSourceItems) GetIntd(key string, dv int) int {
	if v := s.Get(key); v == nil {
		return dv
	} else {
		return xcast.ToInt(v)
	}
}
func (s ConfigDataSourceItems) GetInt64(key string) int64 {
	return xcast.ToInt64(s.Get(key))
}
func (s ConfigDataSourceItems) GetInt64d(key string, dv int64) int64 {
	if v := s.Get(key); v == nil {
		return dv
	} else {
		return xcast.ToInt64(v)
	}
}
func (s ConfigDataSourceItems) GetFloat64(key string) float64 {
	return xcast.ToFloat64(s.Get(key))
}
func (s ConfigDataSourceItems) GetFloat64d(key string, dv float64) float64 {
	if v := s.Get(key); v == nil {
		return dv
	} else {
		return xcast.ToFloat64(v)
	}
}
func (s ConfigDataSourceItems) GetStringSlice(key string) []string {
	return xcast.ToStringSlice(key)
}
func (s ConfigDataSourceItems) GetChildd(key string) ConfigDataSourceItems {
	return s.find(splitConfigKey(key)).childs
}

func (s *ConfDataSource) SetConfig(key string, value interface{}) {
	s.items = s.items.SetConfig(splitConfigKey(key), value)
}
func (s *ConfDataSource) find(keys []string) *ConfDataSourceItem {
	return s.items.find(keys)
}
func (s *ConfDataSource) Get(key string) interface{} {
	return s.find(splitConfigKey(key)).value
}
func (s *ConfDataSource) GetString(key string) string {
	return xcast.ToString(s.Get(key))
}
func (s *ConfDataSource) GetStringd(key, dv string) string {
	v := s.GetString(key)
	if v == "" {
		return dv
	}
	return v
}
func (s *ConfDataSource) GetBool(key string) bool {
	return xcast.ToBool(s.Get(key))
}
func (s *ConfDataSource) GetBoold(key string, dv bool) bool {
	if v := s.Get(key); v == nil {
		return dv
	} else {
		return xcast.ToBool(v)
	}
}
func (s *ConfDataSource) GetInt(key string) int {
	return xcast.ToInt(s.Get(key))
}
func (s *ConfDataSource) GetIntd(key string, dv int) int {
	if v := s.Get(key); v == nil {
		return dv
	} else {
		return xcast.ToInt(v)
	}
}
func (s *ConfDataSource) GetInt64(key string) int64 {
	return xcast.ToInt64(s.Get(key))
}
func (s *ConfDataSource) GetInt64d(key string, dv int64) int64 {
	if v := s.Get(key); v == nil {
		return dv
	} else {
		return xcast.ToInt64(v)
	}
}
func (s *ConfDataSource) GetFloat64(key string) float64 {
	return xcast.ToFloat64(s.Get(key))
}
func (s *ConfDataSource) GetFloat64d(key string, dv float64) float64 {
	if v := s.Get(key); v == nil {
		return dv
	} else {
		return xcast.ToFloat64(v)
	}
}
func (s *ConfDataSource) GetStringSlice(key string) []string {
	return xcast.ToStringSlice(s.Get(key))
}
func (s *ConfDataSource) GetChildd(key string) ConfigDataSourceItems {
	return s.find(splitConfigKey(key)).childs
}
