package xconsul

import (
	"fmt"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/conf"
	capi "github.com/hashicorp/consul/api"
	"net"
	"regexp"
)

// 服务注册

func (x *Xconsul) RegisterService(opts ...func(param *capi.AgentServiceRegistration)) error {
	if !x.IsInit {
		return UnInitErr
	}
	agent := x.Client.Agent()
	param := &capi.AgentServiceRegistration{
		Name:    conf.GetStringd("xconsul.register.name", conf.GetStringd(conf.AppNameKey, "test")),
		Port:    getServicePort(),
		Address: getAddress(),
		Tags:    []string{},
	}
	param.ID = fmt.Sprintf("%s_%d", param.Address, param.Port)
	for _, t := range opts {
		t(param)
	}
	for _, t := range withConfTag() {
		param.Tags = append(param.Tags, t)
	}
	return agent.ServiceRegister(param)
}

func getServicePort() int {
	return conf.GetIntd("port", 8080)
}

func getAddress() string {
	address := conf.GetStringd("privateIp", "")
	if address != "" && isLegalIp(address) {
		return address
	}
	return getLocalIp()
}

// 判断是否是合法的ip地址
func isLegalIp(ip string) bool {
	rule := "^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$"
	if ok, _ := regexp.MatchString(rule, ip); ok {
		return true
	}
	return false
}

const defaultLocalIp = "127.0.0.1"

// 获取本机ip
func getLocalIp() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return defaultLocalIp
	}
	for _, address := range addrs {
		// 检查ip地址判断是否回环地址
		if ipNet, ok := address.(*net.IPNet); ok && !ipNet.IP.IsLoopback() {
			if ipNet.IP.To4() != nil {
				return ipNet.IP.String()
			}
		}
	}
	return defaultLocalIp
}

// 从配置中读取服务注册tag
func withConfTag() []string {
	return conf.GetStringSlice("xconsul.register.tags")
}

func WithDefaultRoute(pathPrefix string) func(param *capi.AgentServiceRegistration) {
	return func(param *capi.AgentServiceRegistration) {
		if pathPrefix == "" {
			pathPrefix = "/" + param.Name
		}
		if pathPrefix[0] != '/' {
			pathPrefix = "/" + pathPrefix
		}
		param.Tags = append(param.Tags, fmt.Sprintf("traefik.http.routers.%s.rule=PathPrefix(`%s`)", param.Name, pathPrefix))
	}
}
