package xconsul

import (
	"fmt"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/conf"
	capi "github.com/hashicorp/consul/api"
)

var UnInitErr = fmt.Errorf("xconsul未初始化")

type Xconsul struct {
	IsInit bool
	*capi.Client
}

func (x *Xconsul) Initialize() error {
	// 获取xconsul的配置
	addr := conf.GetStringd("xconsul.addr", "")
	if addr == "" {
		return nil
	}
	cliConfig := capi.DefaultConfig()
	cliConfig.Address = addr
	client, err := capi.NewClient(cliConfig)
	if err != nil {
		return err
	}
	x.Client = client
	x.IsInit = true
	return nil
}
