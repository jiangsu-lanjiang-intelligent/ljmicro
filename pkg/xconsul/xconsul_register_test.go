package xconsul

import (
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/conf"
	"os"
	"testing"
	"time"
)

func TestXconsul_RegisterService(t *testing.T) {
	os.Setenv("config.file.path", "../../example/config.yaml")
	conf.Initialize()
	if err := Initialize(); err != nil {
		t.Error(err)
		return
	}
	if err := RegisterService(); err != nil {
		t.Error(err)
		return
	}
	time.Sleep(time.Second * 100)
}
