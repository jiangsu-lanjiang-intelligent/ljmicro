package xconsul

import (
	"fmt"
	capi "github.com/hashicorp/consul/api"
)

var defaultXconsul = &Xconsul{}

func Initialize() error {
	fmt.Println("开始初始化【xconsul】基础组件")
	return defaultXconsul.Initialize()
}

func RegisterService(opts ...func(param *capi.AgentServiceRegistration)) error {
	return defaultXconsul.RegisterService(opts...)
}
