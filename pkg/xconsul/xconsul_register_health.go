package xconsul

import (
	"fmt"
	capi "github.com/hashicorp/consul/api"
)

// WithHealthCheck 增加consul健康检查回调函数
// healthPath: 健康检查路径 默认为 /health
func WithHealthCheck(healthPath string) func(param *capi.AgentServiceRegistration) {
	if healthPath == "" {
		healthPath = "/health"
	}
	if healthPath[0] != '/' {
		healthPath = "/" + healthPath
	}
	return func(param *capi.AgentServiceRegistration) {
		// 增加consul健康检查回调函数
		check := new(capi.AgentServiceCheck)
		check.HTTP = fmt.Sprintf("http://%s:%d%s", param.Address, param.Port, healthPath)
		check.Timeout = "5s"                        //超时
		check.Interval = "2s"                       //健康检查频率
		check.DeregisterCriticalServiceAfter = "5s" // 故障检查失败30s后 consul自动将注册服务删除
		param.Check = check
	}
}
