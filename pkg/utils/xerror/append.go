package xerror

import (
	"errors"
	"go.uber.org/multierr"
)

func Append(left string, right error) error {
	return multierr.Append(errors.New(left), right)
}
