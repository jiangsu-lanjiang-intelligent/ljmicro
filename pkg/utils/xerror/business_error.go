package xerror

import "fmt"

type BusinessError struct {
	Code string `json:"code"`
	Msg  string `json:"msg"`
}

func NewBusinessError(code string, msg string) *BusinessError {
	return &BusinessError{
		Code: code,
		Msg:  msg,
	}
}
func (e *BusinessError) Error() string {
	return fmt.Sprintf("%s code=%s", e.Msg, e.Code)
}
