package xerror

type IError interface {
	Error() string
	Detail() string
	Info() string
}
