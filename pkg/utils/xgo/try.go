package xgo

import (
	"errors"
	"fmt"
	"runtime"
)

func Try(fn func() error) (ret error) {
	defer func() {
		if err := recover(); err != nil {
			_, file, line, _ := runtime.Caller(2)
			ret = errors.New(fmt.Sprintf("recover %s:%d %+v", file, line, err))
		}
	}()
	return fn()
}
