package application

/*
IService is the interface that wraps the basic StartUp and Stop methods.
- Name() 服务的名称
- StartUp() 启动服务
- Stop() 停止服务
*/
type IService interface {
	Name() string
	StartUp() error
	Stop()
}
