package application

import (
	"fmt"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/conf"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

var (
	assemblies []IAssembly
	initOnce   = sync.Once{}
	services   []IService
)

// AppendAssemblies 添加系统组件
func AppendAssemblies(a ...IAssembly) {
	assemblies = append(assemblies, a...)
}

// Initialize 初始化应用
func Initialize(name string) {
	initOnce.Do(func() {
		fmt.Println("开始初始化应用 ...")
		conf.SetAppName(name)
		fmt.Println("--------------------------")
		for _, a := range assemblies {
			if err := a(); err != nil {
				panic(err)
			}
			fmt.Println("--------------------------")
		}
		fmt.Println("应用初始化成功 ")
	})
	return
}

// StartUpService 启动服务
func StartUpService(p IService) {
	services = append(services, p)
	go func() {
		fmt.Println("启动服务：" + p.Name())
		if err := p.StartUp(); err != nil {
			panic(err)
		}
	}()
}

// KeepAlive 应用保活
func KeepAlive() {
	/*
					监听系统信号
		SIGHUP = 终端控制进程结束(终端连接断开)
		SIGQUIT = 用户发送QUIT字符(Ctrl+/)触发
		SIGTERM = 结束程序(可以被捕获、阻塞或忽略)
		SIGINT = 用户发送INTR字符(Ctrl+C)触发
	*/

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGHUP, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGINT)
	// 配置超时，5分钟若还没停止完成则强行结束服务
	<-c
	wg := sync.WaitGroup{}
	wg.Add(len(services))
	for _, p := range services {
		go func() {
			p.Stop()
			fmt.Println(p.Name() + " 已停止")
			wg.Done()
		}()
	}
	doneCh := make(chan bool, 1)
	go func() {
		wg.Wait()
		doneCh <- true
	}()

	select {
	case <-doneCh:
		fmt.Println("服务已全部停止")
		os.Exit(0)
	case <-time.After(5 * time.Minute):
		fmt.Println("服务停止超时，强制退出")
		os.Exit(0)
	}

}
