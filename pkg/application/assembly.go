package application

// IAssembly is the interface that wraps the basic Assembly method.
type IAssembly func() error
