package loger

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"strconv"
)

const (
	LableKeyTraceId = "trace_id"
	LableKeySpanId  = "span_id"
	LableKeyReqId   = "req_id"
)

func Initialize() error {
	fmt.Println("开始初始化 log ...")
	return InitializeLoki()
}

/*
Loger 日志记录器
labels 用于记录日志的标签
args 用于记录日志的参数 msg (arg)k=(arg)v
*/
type Loger struct {
	labels map[string]interface{}
	args   []interface{}
}

func (l *Loger) Clone() *Loger {
	copy := &Loger{
		args:   l.args,
		labels: map[string]interface{}{},
	}
	for k, v := range l.labels {
		copy.labels[k] = v
	}
	return copy
}

func (l *Loger) WithLabelMap(args map[string]interface{}) *Loger {
	for k, v := range args {
		l.labels[k] = v
	}
	return l
}

func (l *Loger) WithArg(args ...interface{}) *Loger {
	l.args = append(l.args, args...)
	return l
}

func (l *Loger) WithLabel(k string, v interface{}) *Loger {
	l.labels[k] = v
	return l
}

func (l *Loger) WithContext(ctx context.Context) *Loger {
	if v := ctx.Value(LableKeyReqId); v != nil {
		l.WithLabel(LableKeyReqId, v)
	}
	if v := ctx.Value(LableKeyTraceId); v != nil {
		l.WithLabel(LableKeyTraceId, v)
	}
	if v := ctx.Value(LableKeySpanId); v != nil {
		l.WithLabel(LableKeySpanId, v)
	}
	return l
}

func (l *Loger) handWith(args ...interface{}) string {
	argMsg := ""
	if len(l.args) > 0 {
		args = append(args, l.args...)
	}
	for i := 0; i < len(args); {
		var (
			k string
			v interface{}
		)
		if i+1 >= len(args) {
			k = "unkown"
			v = args[i]
			i++
		} else {
			if tk, ok := args[i].(string); ok {
				k = tk
				v = args[i+1]
				i = i + 2
			} else {
				k = "unkown" + strconv.Itoa(i+1)
				v = args[i]
				i++
			}
		}
		argMsg += fmt.Sprintf(" %s=%v", k, v)
	}
	return argMsg
}
func (l *Loger) Info(msg string, keysAndValues ...interface{}) {
	msg += l.handWith(keysAndValues...)
	logrus.WithFields(l.labels).Info(msg)
}
func (l *Loger) Warn(msg string, keysAndValues ...interface{}) {
	msg += l.handWith(keysAndValues...)
	logrus.WithFields(l.labels).Warn(msg)
}
func (l *Loger) Error(msg string, keysAndValues ...interface{}) {
	msg += l.handWith(keysAndValues...)
	logrus.WithFields(l.labels).Error(msg)
}
