package loger

import (
	"context"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/conf"
)

//	func WithContext(ctx context.Context) *Loger {
//		return GetLoger().WithContext(ctx)
//	}
func GetLoger() *Loger {
	l := &Loger{
		labels: make(map[string]interface{}),
	}

	var (
		appEnv  = conf.GetString(conf.AppEnvKey)
		appName = conf.GetString(conf.AppNameKey)
	)
	if appName != "" {
		l.WithLabel("app_name", appName)
	}
	if appEnv != "" {
		l.WithLabel("app_env", appEnv)
	}
	return l
}

func Info(msg string, keysAndValues ...interface{}) {
	GetLoger().Info(msg, keysAndValues...)
}
func Warn(msg string, keysAndValues ...interface{}) {
	GetLoger().Warn(msg, keysAndValues...)
}
func Error(msg string, keysAndValues ...interface{}) {
	GetLoger().Error(msg, keysAndValues...)
}
func WithContext(ctx context.Context) *Loger {
	return GetLoger().WithContext(ctx)
}
func WithLabelMap(arg map[string]interface{}) *Loger {
	return GetLoger().WithLabelMap(arg)
}
