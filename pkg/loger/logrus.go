package loger

import (
	"github.com/sirupsen/logrus"
)

func init() {
	//设置日志format
	logrus.SetFormatter(&logrus.JSONFormatter{})
}
