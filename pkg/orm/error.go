package orm

import (
	"fmt"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/utils/xerror"
)

type DbError struct {
	err error
}

func (e *DbError) Error() string {
	return e.err.Error()
}

func (e *DbError) Detail() string {
	return fmt.Sprintf("数据库异常，请稍后重试[%s]", e.err.Error())
}

func (e *DbError) Info() string {
	return "数据库异常，请稍后重试"
}

func NewDbError(err error) xerror.IError {
	return &DbError{
		err: err,
	}
}
