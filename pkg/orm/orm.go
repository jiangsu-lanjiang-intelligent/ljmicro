package orm

import (
	"context"
	"fmt"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/conf"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/loger"
	"gorm.io/gorm"
	"time"
)

var defaultDb *gorm.DB

func Initialize() (err error) {
	fmt.Println("开始初始化 orm ...")
	//语法https://www.kancloud.cn/sliver_horn/gorm/1861155
	dsn := conf.GetStringd("orm.dsn", "")
	if dsn == "" {
		fmt.Println("[Warn] DSN is empty skip initialize orm")
		return nil
	}
	db, err := NewOrm(dsn)
	if err != nil {
		return err
	}
	defaultDb = db
	return nil
}

func NewOrm(dsn string) (*gorm.DB, error) {
	// 解析dsn
	dialector, err := ParseOrmDsn(dsn)
	if err != nil {
		return nil, err
	}
	orm, err := gorm.Open(dialector, &gorm.Config{
		Logger: &ormLoger{
			Loger:       loger.GetLoger().WithLabel("module", "orm"),
			isNeedTrace: conf.GetBoold("orm.is_need_trace", true),
		},
	})
	if err != nil {
		return nil, err
	}
	db, err := orm.DB()
	if err != nil {
		return nil, err
	}
	db.SetMaxOpenConns(conf.GetIntd("orm.conn.open.max", 50))
	db.SetMaxIdleConns(conf.GetIntd("orm.conn.idle.max", 50))
	db.SetConnMaxLifetime(time.Duration(conf.GetIntd("orm.conn.lifetime.max", 10)) * time.Second)
	return orm, nil
}

func GetDB() *gorm.DB {
	if defaultDb != nil {
		return defaultDb
	}
	panic("please init db first")
}

func GetDBWithCtx(ctx context.Context) *gorm.DB {
	return GetDB().WithContext(ctx)
}
