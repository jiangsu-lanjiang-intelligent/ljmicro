package orm

import (
	"gorm.io/gorm"
	"time"
)

type (
	BasePageResp struct {
		Count int64       `json:"count"`
		Data  interface{} `json:"data"`
	}
	BasePageReq struct {
		Page int `json:"page"`
		Size int `json:"size"`
	}
)
type BaseModel struct {
	Id        int            `json:"id" gorm:"primary_key"`
	Uuid      string         `json:"uuid" gorm:"index"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	Deleted   gorm.DeletedAt `json:"deleted" gorm:"index"`
}
type SimpleBaseModel struct {
	Id        int            `json:"id" gorm:"primary_key"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	Deleted   gorm.DeletedAt `json:"deleted" gorm:"index"`
}
