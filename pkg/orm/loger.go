package orm

import (
	"context"
	"fmt"
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/loger"
	"gorm.io/gorm/logger"
	"time"
)

type ormLoger struct {
	isNeedTrace bool
	*loger.Loger
}

func (l *ormLoger) LogMode(logger.LogLevel) logger.Interface {
	return l
}
func (l *ormLoger) Info(ctx context.Context, msg string, arg ...interface{}) {
	l.Clone().WithContext(ctx).Info(fmt.Sprintf(msg, arg...))
}
func (l *ormLoger) Warn(ctx context.Context, msg string, arg ...interface{}) {
	l.Clone().WithContext(ctx).Warn(fmt.Sprintf(msg, arg...))
}
func (l *ormLoger) Error(ctx context.Context, msg string, arg ...interface{}) {
	l.Clone().WithContext(ctx).Error(fmt.Sprintf(msg, arg...))
}
func (l *ormLoger) Trace(ctx context.Context, begin time.Time, fc func() (sql string, rowsAffected int64), err error) {
	if l.isNeedTrace {
		sql, rows := fc()
		msg := fmt.Sprintf("[trace] %s %d", sql, rows)
		if err != nil {
			msg = fmt.Sprintf("%s error=%s", msg, err.Error())
		}
		l.Clone().WithContext(ctx).Info(msg)
	}
}
