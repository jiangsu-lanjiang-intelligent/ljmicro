package orm

import (
	"gitee.com/jiangsu-lanjiang-intelligent/ljmicro/pkg/utils/xerror"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

type Option func(*gorm.DB) *gorm.DB

func SearchOption_UUID(uuid string) Option {
	return func(db *gorm.DB) *gorm.DB {
		db.Where("uuid = ?", uuid)
		return db
	}
}
func Option_Model(m schema.Tabler) Option {
	return func(db *gorm.DB) *gorm.DB {
		db.Model(m)
		return db
	}
}
func Option_Table(tableName string) Option {
	return func(db *gorm.DB) *gorm.DB {
		db.Table(tableName)
		return db
	}
}
func DoOption(db *gorm.DB, opts ...Option) *gorm.DB {
	db = db.Session(&gorm.Session{})
	for _, t := range opts {
		db = t(db)
	}
	return db
}
func GetFirstRecord(db *gorm.DB, in interface{}, options ...Option) xerror.IError {
	db = DoOption(db, options...)
	if err := db.First(in).Error; err != nil {
		return NewDbError(err)
	}
	return nil
}
func GetListRecord(db *gorm.DB, in interface{}, options ...Option) xerror.IError {
	db = DoOption(db, options...)
	if err := db.Find(in).Error; err != nil {
		return NewDbError(err)
	}
	return nil
}

func GetPageListRecord(db *gorm.DB, page, size int, in interface{}, options ...Option) (*BasePageResp, xerror.IError) {
	data := &BasePageResp{
		Data: in,
	}
	db = DoOption(db, options...)
	db.Count(&data.Count)
	db.Limit(size).Offset((page - 1) * size)
	if err := GetListRecord(db, in); err != nil {
		return nil, NewDbError(err)
	}
	return data, nil
}
func GetPageListRecord2(db *gorm.DB, page *BasePageReq, in interface{}, options ...Option) (*BasePageResp, xerror.IError) {
	return GetPageListRecord(db, page.Page, page.Size, in, options...)
}

func Create(db *gorm.DB, in interface{}) xerror.IError {
	if err := db.Create(in).Error; err != nil {
		return NewDbError(err)
	}
	return nil
}

func Updates(db *gorm.DB, in interface{}, options ...Option) xerror.IError {
	db = DoOption(db, options...)
	if err := db.Updates(in).Error; err != nil {
		return NewDbError(err)
	}
	return nil
}

func Delete(db *gorm.DB, table interface{}, options ...Option) xerror.IError {
	db = DoOption(db, options...)
	if err := db.Delete(table).Error; err != nil {
		return NewDbError(err)
	}
	return nil
}
