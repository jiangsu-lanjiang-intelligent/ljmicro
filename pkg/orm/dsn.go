package orm

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func ParseOrmDsn(dsn string) (gorm.Dialector, error) {
	return mysql.Open(dsn), nil
}
